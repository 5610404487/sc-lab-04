
public class LoopController {
	private NestedLoop nloop  = new NestedLoop();
	private int indextype;
	public int getIndex(int index){
		indextype = index;
		return indextype;
	}
	public String getOne(int n){
		return nloop.typeOne(n);
	}
	public String getTwo(int n){
		return nloop.typeTwo(n);
	}
	public String getThree(int n){
		return nloop.typeThree(n);
	}
	public String getFour(int n){
		return nloop.typeFour(n);
	}
	public String getClearA(){
		return nloop.clearA();
	}
}
