
public class NestedLoop {
	private int i;
	private int j;
	String a = "";
	
	public String typeOne(int n){
		for (i=1; i<=n; i++){    
			for(j=1; j<=n; j++){
				a=a+"*";
			}
			a=a+"\n";
		}
		a=a+"\n";
		return a;
	}
	public String typeTwo(int n){
		for (i=1; i<=n; i++){      
			for (j=1; j<=i; j++){
				a=a+"*";
			}
			a=a+"\n";
		}
		a=a+"\n";
		return a;
	}
	public String typeThree(int n){
		for (i=1; i<=n; i++){    
			for (j=1; j<=5; j++){
				if(j%2==0){
					a=a+"*";
				}
				else{
					a=a+"-";
				}
			}
			a=a+"\n";
		}
		a=a+"\n";
		return a;
	}
	public String typeFour(int n){
		for (i=1; i<=n; i++){
			for (j=1; j<=5; j++){
				if((i+j)%2==0){
					a=a+"*";
				}
				else{
					a=a+" ";
				}
			}
			a=a+"\n";
		}
		a=a+"\n";
		return a;
	}
	public String clearA(){
		a = "";
		return a;
	}
}
