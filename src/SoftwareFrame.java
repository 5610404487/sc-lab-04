import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


public class SoftwareFrame {
	LoopController con = new LoopController();
	public static void main(String args[]){
		new SoftwareFrame();
	}
	public SoftwareFrame(){
		JFrame f = new JFrame();
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();

		p1.setPreferredSize(new Dimension(250,300));
		p2.setPreferredSize(new Dimension(235,300));
		p1.setBackground(Color.lightGray);
		p2.setBackground(Color.pink);
		p1.setBorder(new TitledBorder(new EtchedBorder(), "Nested Loops"));
		p2.setBorder(new TitledBorder(new EtchedBorder(), "Select Type"));

		JTextField tf1 = new JTextField();
		JTextArea ta1 = new JTextArea();
		tf1.setPreferredSize(new Dimension(100,70));
		ta1.setPreferredSize(new Dimension(200,280));
		JComboBox c1 = new JComboBox();
		JButton submit = new JButton("Run");
		JLabel label1 = new JLabel("Input N: ", SwingConstants.CENTER);
		submit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				String text = " ";
				int index = c1.getSelectedIndex();
				index = index+1;
				int ntype = con.getIndex(index);
				String strN = tf1.getText();
				int n = Integer.parseInt(strN);
				if (ntype == 1){
					text = con.getOne(n);
				}
				if (ntype == 2){
					text = con.getTwo(n);
				}
				if (ntype == 3){
					text = con.getThree(n);
				}
				if (ntype == 4){
					text = con.getFour(n);
				}
				ta1.setText(text);
			}
		});
		c1.addItem("Ẻ��� 1 --------------------------");
		c1.addItem("Ẻ��� 2 --------------------------");
		c1.addItem("Ẻ��� 3 --------------------------");
		c1.addItem("Ẻ��� 4 --------------------------");
		ta1.setText("-------Nested Loops --------------"+"\n"+"\n");
		p2.add(c1,BorderLayout.NORTH);
		p2.add(label1,BorderLayout.CENTER);
		p2.add(tf1,BorderLayout.CENTER);
		p2.add(submit,BorderLayout.SOUTH);
		
		p1.add(ta1,BorderLayout.CENTER);
		f.add(p1, BorderLayout.WEST);
		f.add(p2, BorderLayout.EAST);
		f.setSize(500, 350);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().setLayout(null);
	}
}
